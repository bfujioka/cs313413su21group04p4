package edu.luc.etl.cs313.android.simplestopwatch.test.model.state;

import android.database.DatabaseErrorHandler;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.luc.etl.cs313.android.simplestopwatch.R;
import edu.luc.etl.cs313.android.simplestopwatch.common.StopwatchUIUpdateListener;
import edu.luc.etl.cs313.android.simplestopwatch.model.clock.ClockModel;
import edu.luc.etl.cs313.android.simplestopwatch.model.clock.OnTickListener;
import edu.luc.etl.cs313.android.simplestopwatch.model.state.StopwatchStateMachine;
import edu.luc.etl.cs313.android.simplestopwatch.model.time.TimeModel;

/**
 * Testcase superclass for the stopwatch state machine model. Unit-tests the state
 * machine in fast-forward mode by directly triggering successive tick events
 * without the presence of a pseudo-real-time clock. Uses a single unified mock
 * object for all dependencies of the state machine model.
 *
 * @author laufer
 // @see http://xunitpatterns.com/Testcase%20Superclass.html
 */


// TODO add in needed test parts
public abstract class AbstractStopwatchStateMachineTest {

    private StopwatchStateMachine model;

    private UnifiedMockDependency dependency;

    @Before
    public void setUp() throws Exception {
        dependency = new UnifiedMockDependency();
    }

    @After
    public void tearDown() {
        dependency = null;
    }

    /**
     * Setter for dependency injection. Usually invoked by concrete testcase
     * subclass.
     *
     // @param model
     */
    protected void setModel(final StopwatchStateMachine model) {
        this.model = model;
        if (model == null)
            return;
        this.model.setUIUpdateListener(dependency);
        this.model.actionInit();
    }

    protected UnifiedMockDependency getDependency() {
        return dependency;
    }

    /**
     * Verifies that we're initially in the stopped state (and told the listener
     * about it).
     */
    @Test
    public void testPreconditions() {
        assertEquals(R.string.STOPPED, dependency.getState());
    }

    /**
     * Verifies the following scenario: time is 0, press start, wait 5+ seconds,
     * expect time 5.
     */

    @Test
    public void testScenarioCountMax() {
        dependency.updateTime(99);
        assertTimeEquals(99);
        dependency.incRuntime();
        assertTimeEquals(99);
    }
    @Test
    public void testScenarioIncrement(){
        assertEquals(R.string.STOPPED, dependency.getState());
        assertFalse(dependency.isStarted());
        model.onStartStop();
        assertEquals(R.string.INCREMENTING, dependency.getState());
        model.onStartStop();
        assertTimeEquals(+1);
    }
    @Test
    public void testScenarioDecrement() throws Throwable {
        assertEquals(R.string.STOPPED, dependency.getState());
        assertFalse(dependency.isStarted());
        dependency.updateTime(1);
        assertTimeEquals(1);
        dependency.start();
        dependency.updateState(R.string.RUNNING);
        assertEquals(R.string.RUNNING, dependency.getState());
        assertTrue(dependency.isStarted());
        model.onTick();
        assertTimeEquals(0);
    }
    @Test
    public void testScenarioAlarm() throws Throwable {
        dependency.updateTime(5);
        assertTimeEquals(5);
        model.toRunningState();
        assertEquals(R.string.RUNNING, dependency.getState());
        model.onTick();
        assertTimeEquals(0);
        assertEquals(R.string.ALARMING, dependency.getState());
    }
    @Test
    public void testScenarioDelayStart() throws Throwable {
        dependency.updateState(5);
        assertTimeEquals(5);

        //timer beeps once
        assertEquals(R.string.RUNNING, dependency.getState());
        assertTrue(dependency.isStarted());
    }
    @Test
    public void testScenarioAlarmOff(){
        model.toAlarmState();
        assertEquals(R.string.ALARMING, dependency.getState());
        model.onStartStop();
        assertTimeEquals(0);
        assertEquals(R.string.STOPPED, dependency.getState());
    }
    @Test
    public void testScenarioCountMin() throws Throwable {
        dependency.updateTime(5);
        assertTimeEquals(5);
        dependency.start();
        dependency.updateState(R.string.RUNNING);
        assertEquals(R.string.RUNNING, dependency.getState());
        assertTrue(dependency.isStarted());
        model.onTick();
        assertTimeEquals(0);
    }
    @Test
    public void testScenarioReset() {
        model.toRunningState();
        assertEquals(R.string.RUNNING, dependency.getState());
        model.onStartStop();
        assertTimeEquals(0);
        assertEquals(R.string.STOPPED, dependency.getState());
    }

    /**
     * Sends the given number of tick events to the model.
     *
     *  @param n the number of tick events
     */
    protected void onTickRepeat(final int n) {
        for (int i = 0; i < n; i++)
            model.onTick();
    }

    /**
     * Checks whether the model has invoked the expected time-keeping
     * methods on the mock object.
     */
    protected void assertTimeEquals(final int t) {
        assertEquals(t, dependency.getTime());
    }
}

/**
 * Manually implemented mock object that unifies the three dependencies of the
 * stopwatch state machine model. The three dependencies correspond to the three
 * interfaces this mock object implements.
 *
 * @author laufer
 */
class UnifiedMockDependency implements TimeModel, ClockModel, StopwatchUIUpdateListener {

    private int timeValue = -1, stateId = -1;

    private int runningTime = 0;

    private boolean started = false;

    public int getTime() {
        return timeValue;
    }

    public int getState() {
        return stateId;
    }

    public boolean isStarted() {
        return started;
    }

    @Override
    public void decRuntime(){
        runningTime --;
    }
    @Override
    public void updateTime(final int timeValue) {
        this.timeValue = timeValue;
    }

    @Override
    public void updateState(final int stateId) {
        this.stateId = stateId;
    }

    @Override
    public void alarmRing() {
    }

    @Override
    public void setOnTickListener(OnTickListener listener) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void start() {
        started = true;
    }

    @Override
    public void stop() {
        started = false;
    }

    @Override
    public void resetRuntime() {
        runningTime = 0;
    }


    @Override
    public void incRuntime() {
            runningTime++;
    }

    @Override
    public int getRuntime() {
        return runningTime;
    }

    //Override Alarm
    //TODO add alarm?
    //TODO Remove dependencies for laps etc.
}
