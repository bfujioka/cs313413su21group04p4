package edu.luc.etl.cs313.android.simplestopwatch.model.state;

import edu.luc.etl.cs313.android.simplestopwatch.R;

public class AlarmState implements StopwatchState {

    public AlarmState (final StopwatchSMStateView sm) {
        this.sm = sm;
    }

    private  final StopwatchSMStateView sm;


    @Override
    public void updateView() {
        sm.updateUIRuntime();
    }

    @Override
    public int getId() {
        return R.string.ALARMING;
    }

    @Override
    public void onStartStop() {
        sm.actionStop();
        sm.toStoppedState();
    }

    @Override
    public void onTick() {
        sm.actionAlarm();
    }

}
