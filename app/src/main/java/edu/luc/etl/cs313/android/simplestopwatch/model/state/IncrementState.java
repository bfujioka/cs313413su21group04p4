package edu.luc.etl.cs313.android.simplestopwatch.model.state;
import edu.luc.etl.cs313.android.simplestopwatch.R;


class IncrementState implements StopwatchState {

    public IncrementState(final StopwatchSMStateView sm) {
        this.sm = sm;
    }

    private final StopwatchSMStateView sm;

    private int tickCount = 0;

    public void onStartStop() {
        if (sm.getTime() == 99) {
            sm.toRunningState();
        } else {
            tickCount = 0;
            sm.actionInc();
        }
    }

    public void onTick() {
        if (tickCount == 3 && sm.getTime()!= 0) {
            sm.toRunningState();
        } else {
            tickCount++;
        }
    }

    public void updateView() {
        sm.updateUIRuntime();
    }

    public int getId() {
        return R.string.INCREMENTING;
    }

}
